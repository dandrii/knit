import moment from 'moment-timezone';

moment.tz.setDefault('Europe/Kiev');

export const c = [
	'08.30 - 09.50',
	'10.00 - 11.20',
	'11.30 - 12.50',
	'13.20 - 14.40',
	'14.50 - 16.10'
];

export const schedule: Schedule = {
	'ІПЗ': [
		[
			{
				name: 'Фіз.виховання'
			},
			{
				name: 'Іноземна мова',
				room: 'Л.102'
			},
			[
				{
					name: 'Фізика',
					room: 'Л.317'
				},
				null
			],
			[
				{
					name: 'Історія держ.',
					room: 'Л.3'
				},
				null
			]
		],
		[
			null,
			{
				name: 'Лінійна алгебра',
				room: 'Л.304'
			},
			{
				name: 'Фізика',
				room: 'Л.322'
			},
			{
				name: 'Фізика',
				room: 'Л.3'
			}
		],
		[
			null,
			{
				name: 'Історія укр. держ.',
				room: 'Л.32'
			},
			{
				name: 'Осн.акад.письма',
				room: 'Л.313'
			},
			{
				name: 'Алгор.і структури',
				room: 'П.211/П.18'
			},
			[
				{
					name: 'Алгор.і структури',
					room: 'П.212а'
				},
				null
			]
		],
		[
			null,
			{
				name: 'Фіз.виховання'
			},
			{
				name: 'Лінійна алгебра',
				room: 'Л.320'
			},
			{
				name: 'Осн.програмування',
				room: 'П.18/П.213'
			},
			{
				name: 'Алгор.і структури',
				room: 'П.212а'
			},
		],
		[
			[
				null,
				{
					name: 'Осн.програмування',
					room: 'П.44'
				},
			],
			[
				null,
				{
					name: 'Осн.програмування',
					room: 'П.219'
				},
			]
		]
	],
	'КСМ': [
		[
			{
				name: 'Дискретна матем.',
				room: 'Л.411'
			},
			{
				name: 'Фіз.виховання'
			},
			{
				name: 'Програмування',
				room: 'Л.1'
			},
			[
				{
					name: 'Історія держ.',
					room: 'Л.3'
				},
				{
					name: 'Дискр.матем.',
					room: 'Л.303'
				}
			]
		],
		[
			{
				name: 'Комп. графіка',
				room: 'Л.400'
			},
			[
				{
					name: 'Комп. графіка',
					room: 'Л.400'
				},
				{
					name: 'Дискр.матем.',
					room: 'Л.301'
				}
			],
			[
				{
					name: 'Програмування',
					room: 'Л.303а'
				},
				null
			]
		],
		[
			{
				name: 'Історія укр.держ.',
				room: 'Л.153'
			},
			{
				name: 'Фіз.виховання'
			},
			{
				name: 'Програмування',
				room: 'Л.303а'
			},
			{
				name: 'Іноз. мова нім.',
				room: 'Л.241'
			}
		],
		[
			{
				name: 'Іноземна мова',
				room: 'Л.255'
			},
			{
				name: 'Дискр.матем.',
				room: 'Л.301'
			},
			{
				name: 'Вища.матем.',
				room: 'Л.304'
			}
		],
		[
			{
				name: 'Осн.акад.письма',
				room: 'Л.153'
			},
			{
				name: 'Дискр.матем.',
				room: 'Л.313'
			},
			{
				name: 'Вища.матем.',
				room: 'Л.304'
			}
		]
	]
}

export const week = ['Tu', 'We', 'Th', 'Fr', 'Sa'];

export const getPart = () => {
	const currentWeek = moment().get('week');
	const startWeek = moment('2019-09-01').year(new Date().getFullYear()).get('week');
	const week = currentWeek - startWeek + 1;
	return week % 2 === 0 ? 1 : 0;
};

enum Keys {
	I = 'ІПЗ',
	K = 'КСМ'
};

interface ISubject {
	name: string;
	room?: string;
};

type IDay = (ISubject | [ISubject | null, ISubject | null] | null)[];

type Schedule = {
	[key in Keys]: [IDay, IDay, IDay, IDay, IDay]
};
