import { readFileSync, existsSync } from 'fs';

const NEWLINE = /\n|\r|\r\n/;
const KEY_VAL_PAIR = /^\s*([\w.-]+)\s*=\s*(.*)?\s*$/;

const config: Map<string, string> = new Map();
if (existsSync(process.cwd() + '/.env'))
	readFileSync(process.cwd() + '/.env', 'utf-8').split(NEWLINE).forEach((line) => {
		const pair = line.match(KEY_VAL_PAIR);
		
		if (pair === null)
			return;

		const key = pair[1];
		let value = pair[2];

		if ((value.startsWith('"') || value.startsWith('\'')) && (value.endsWith('"') || value.endsWith('\'')))
			value = value.substring(1, value.length - 1)
		else
			value = value.trim();
		value.replace('\\n', '\n');

		config.set(key, value);
	});

config.forEach((value, key) => {
	if (Object.prototype.hasOwnProperty.call(process.env, value))
		return;

	process.env[key] = value;
});
